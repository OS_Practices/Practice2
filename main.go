package main

import (
	"crypto/sha256"
	"flag"
	"fmt"
	"log"
	"math"
	"sync"
	"time"
)

var (
	hash    string
	thCount int
)

var dict = make(map[int]rune)

const maxBase26 = 11881375

func init() {
	flag.Usage = func() {
		fmt.Println("This program search decrypt string.")
		fmt.Println("Programm syntax: -c [count thread] [hash]")
		flag.PrintDefaults()
	}
	flag.IntVar(&thCount, "c", 1, "Count thread to decrypt string")
	flag.Parse()
	hash = flag.Arg(0)

	for i := 0; i < 26; i++ {
		dict[i] = rune('a' + i)
	}
}
func main() {
	//11881375))
	if thCount >= maxBase26+1 || thCount <= 0 {
		log.Fatal("Error: Very many thread fot bruteforce")
	}

	offset := int(math.Ceil(11881375.0 / float64(thCount)))
	cou := 0

	startTime, endTime := time.Now(), time.Now()
	wg := sync.WaitGroup{}

	fmt.Println("Start time: ", startTime.Format("15:04:05.000"))

	for i := 0; i < maxBase26/offset; i++ {
		wg.Add(1)
		go search(cou, cou+offset, hash, &endTime, &wg)
		cou += offset
	}

	wg.Add(1)
	go search(cou, maxBase26+1, hash, &endTime, &wg)
	wg.Wait()

	fmt.Println("Total time: ", endTime.Sub(startTime))

}

func search(start, end int, hash string, endTime *time.Time, wg *sync.WaitGroup) {
	defer wg.Done()

	for i := start; i < end; i++ {
		bruteStr := []byte(toBase26(i))
		bruteHash := fmt.Sprintf("%x", sha256.Sum256(bruteStr))
		if bruteHash == hash {
			*endTime = time.Now()
			fmt.Println(string(bruteStr), " - ", endTime.Format("15:04:05.000"))
		}
	}
}

func toBase26(num int) string {
	res := []rune("aaaaa")

	for i := 4; i >= 0; i-- {
		res[i] = dict[num%26]
		num /= 26
	}

	return string(res)
}
